---
date: "2025-02-21"
title: "2025-02-20 (2 commits)"
---

## Overview

A couple of `phpcs` related changes.

## Changes

It covers commits from 2025-02-20 on `11.x` branch.

### #3498297: Fix 'Drupal.Commenting.ClassComment.Missing' in test modules

[#3498297][3498297] - [5dc03f8a3defdf47c4e3dbd8b2ad5d4e4accc4be][5dc03f8a3defdf47c4e3dbd8b2ad5d4e4accc4be]

Enables `phpcs`' `Drupal.Commenting.ClassComment.Missing` in test modules and
fixes remaining non-compliant cases.

[3498297]: https://www.drupal.org/project/drupal/issues/3498297
[5dc03f8a3defdf47c4e3dbd8b2ad5d4e4accc4be]: https://git.drupalcode.org/project/drupal/-/commit/5dc03f8a3defdf47c4e3dbd8b2ad5d4e4accc4be

### #3507043: Enable DrupalPractice.Objects.GlobalFunction

[#3507043][3507043] - [6793be991e510790e12fde94edf138f81a7239fc][6793be991e510790e12fde94edf138f81a7239fc]

Turns on `phpcs`'s `DrupalPractice.Objects.GlobalFunction`.

[3507043]: https://www.drupal.org/project/drupal/issues/3507043
[6793be991e510790e12fde94edf138f81a7239fc]: https://git.drupalcode.org/project/drupal/-/commit/6793be991e510790e12fde94edf138f81a7239fc

## Annex

### Meta

- Commits: 2
- Approach:
  - Direct commit: 0
  - Merge request + Patch: 0
  - Merge request: 2
  - Patch: 0
- Shortest running issue: 3 days
- Longest running issue: 1 months
- People: 3
- Volunteers: 0
- Organizations: 3
  - PreviousNext
    - quietone
  - QED42
    - shalini_jha
  - Très Bien Tech
    - nod_
  - Volunteers
- Maintainers accepting changes
  - nod_
- Parent issues
  - [#3105950](https://www.drupal.org/project/drupal/issues/3105950): [meta] Fix 'Drupal.Commenting.ClassComment.Missing' coding standard (1)
  - [#3113904](https://www.drupal.org/project/drupal/issues/3113904): [META] Replace t() calls inside of classes (1)
- Tags
  - strict (2)
  - Coding standards (2)
  - Needs Review Queue Initiative (1)


### The set

    $ git log --oneline --reverse --since=2025-02-19T23:59:59+00:00 --until=2025-02-21T00:00:00+00:00
    6793be991e5 Issue #3507043 by quietone: Enable DrupalPractice.Objects.GlobalFunction
    5dc03f8a3de Issue #3498297 by shalini_jha, quietone: Fix 'Drupal.Commenting.ClassComment.Missing' in test modules

    $ git diff --shortstat 6793be991e5~1..5dc03f8a3de
     49 files changed, 146 insertions(+), 8 deletions(-)
