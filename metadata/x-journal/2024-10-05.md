---
date: "2024-10-07"
title: "2024-10-05 (5 commits)"
---

## Overview

Tests tweaking, styling improvements, and nicer JavaScript for `tabledrag`.

## Changes

It covers commits from 2024-10-05 on `11.x` branch.

### #3478422: Tweak @group #slow for kernel tests again

[#3478422][3478422] - [293db5f25898b3712e4f6caef9fb953793c42495][293db5f25898b3712e4f6caef9fb953793c42495]

Marks a couple more tests as slow.

[3478422]: https://www.drupal.org/project/drupal/issues/3478422
[293db5f25898b3712e4f6caef9fb953793c42495]: https://git.drupalcode.org/project/drupal/-/commit/293db5f25898b3712e4f6caef9fb953793c42495

### #3213995: Olivero: submit button is too wide in the off canvas dialog box

[#3213995][3213995] - [808ceae426b627e954017805090404c42ae27b04][808ceae426b627e954017805090404c42ae27b04]

Makes off-canvas buttons take a more natural width.

[3213995]: https://www.drupal.org/project/drupal/issues/3213995
[808ceae426b627e954017805090404c42ae27b04]: https://git.drupalcode.org/project/drupal/-/commit/808ceae426b627e954017805090404c42ae27b04

### #3470276: Long breadcrumbs are creating scrollbar

[#3470276][3470276] - [2304de785b334d58f2fb466b9f2630eab9be1aa4][2304de785b334d58f2fb466b9f2630eab9be1aa4]

Prevents scroll on breadcrumbs when using `olivero` theme when breadcrumbs
content is long.

[3470276]: https://www.drupal.org/project/drupal/issues/3470276
[2304de785b334d58f2fb466b9f2630eab9be1aa4]: https://git.drupalcode.org/project/drupal/-/commit/2304de785b334d58f2fb466b9f2630eab9be1aa4

### #3478425: Regression in run-tests.sh ordering of unit tests

[#3478425][3478425] - [09f1d69f42e1f50da18cc114ff3fb924073a0f27][09f1d69f42e1f50da18cc114ff3fb924073a0f27]

Improves test running order and tweaks batch wait for improved performance.

[3478425]: https://www.drupal.org/project/drupal/issues/3478425
[09f1d69f42e1f50da18cc114ff3fb924073a0f27]: https://git.drupalcode.org/project/drupal/-/commit/09f1d69f42e1f50da18cc114ff3fb924073a0f27

### #3477740: Prefer to replace all window.pageYOffset/window.pageXOffset with window.scrollY/window.scrollX

[#3477740][3477740] - [40dfb2b06b4a9e3138d4aec8786fac6b1822d8d9][40dfb2b06b4a9e3138d4aec8786fac6b1822d8d9]

Tweaks `drupal.tabledrag` JavaScript to use `window.scrollY` instead of
`window.pageYOffset`, which is an alias of the former.

[3477740]: https://www.drupal.org/project/drupal/issues/3477740
[40dfb2b06b4a9e3138d4aec8786fac6b1822d8d9]: https://git.drupalcode.org/project/drupal/-/commit/40dfb2b06b4a9e3138d4aec8786fac6b1822d8d9

## Annex

### Meta

- Commits: 5
- Approach:
  - Direct commit: 0
  - Merge request + Patch: 3
  - Merge request: 2
  - Patch: 0
- Shortest running issue: 2 days
- Longest running issue: 3 years
- People: 11
- Volunteers: 2
- Organizations: 12
  - Acquia
    - bnjmnm
    - lauriii
  - Axelerant
    - gauravvvv
  - DICTU
    - groendijk
  - DevBranch
    - smovs
  - Drupal Ukraine Community
    - smovs
  - Kanopi Studios
    - cindytwilliams
  - Material
    - nayana_mvr
  - Mobomo
    - smustgrave
  - OpenSense Labs
    - gauravvvv
  - Third and Grove
    - catch
  - Très Bien Tech
    - nod_
  - Volunteers
    - nod_
    - spuky
- Maintainers accepting changes
  - nod_
- Tags
  - tests (2)
  - Test suite performance (2)
  - Needs Review Queue Initiative (2)
  - theme (1)
  - styling (1)
  - olivero (1)
  - Novice (1)
  - javascript (1)
  - DrupalCon Lille 2023 (1)
  - Bug Smash Initiative (1)
  - Barcelona2024 (1)


### The set

    $ git log --oneline --reverse --since=2024-10-04T23:59:59+00:00 --until=2024-10-06T00:00:00+00:00
    40dfb2b06b Issue #3477740 by tom konda, smustgrave: Prefer to replace all window.pageYOffset/window.pageXOffset with window.scrollY/window.scrollX
    09f1d69f42 Issue #3478425 by catch, mondrake: Regression in run-tests.sh ordering of unit tests
    2304de785b Issue #3470276 by spuky, nayana_mvr, smustgrave: To long Breadcrumbs are creating scrollbar
    808ceae426 Issue #3213995 by groendijk, bnjmnm, gauravvvv, smovs, cindytwilliams, smustgrave, lauriii: Olivero: submit button is too wide in the off canvas dialog box
    293db5f258 Issue #3478422 by catch: Tweak @group #slow for kernel tests again

    $ git diff --shortstat 40dfb2b06b~1..293db5f258
     17 files changed, 16 insertions(+), 21 deletions(-)
