---
date: "2024-10-01"
title: "2024-09-30 (2 commits)"
---

## Overview

One extra Twig deprecation fix, and an external URL value update.

## Changes

It covers commits from 2024-09-30 on `11.x` branch.

### #3462868: Replace url usage of http://www.w3.org with https://www.w3.org in core

[#3462868][3462868] - [9ba6f7441760f9739a599176903029489ab1f193][9ba6f7441760f9739a599176903029489ab1f193]

Replaces W3C URLs to point to HTTPS instead of HTTP.

[3462868]: https://www.drupal.org/project/drupal/issues/3462868
[9ba6f7441760f9739a599176903029489ab1f193]: https://git.drupalcode.org/project/drupal/-/commit/9ba6f7441760f9739a599176903029489ab1f193

### #3477373: Fix "Not passing an instance of "TwigFunction" when creating a function of type "FunctionExpression" is deprecated."

[#3477373][3477373] - [4b7624537e18fa0da655c9aacfb5124c5fb0c355][4b7624537e18fa0da655c9aacfb5124c5fb0c355]

Addresses one more Twig 3.12 deprecation.

[3477373]: https://www.drupal.org/project/drupal/issues/3477373
[4b7624537e18fa0da655c9aacfb5124c5fb0c355]: https://git.drupalcode.org/project/drupal/-/commit/4b7624537e18fa0da655c9aacfb5124c5fb0c355

## Annex

### Meta

- Commits: 2
- Approach:
  - Direct commit: 0
  - Merge request + Patch: 1
  - Merge request: 1
  - Patch: 0
- Shortest running issue: 2 days
- Longest running issue: 2 months
- People: 13
- Volunteers: 5
- Organizations: 13
  - Acquia
    - ankitv18
  - DrupalEasy
    - ultimike
  - Full Fat Things
    - longwave
  - Innoraft
    - sheetal-wish
  - Mobomo
    - smustgrave
  - PreviousNext
    - quietone
  - QED42
    - annmarysruthy
  - SWIS
    - bbrala
  - Skilld
    - finnsky
  - Suntory Global Spirits
    - rodrigoaguilera
  - Zivtech
    - yujiman85
  - Zoocha
    - nexusnovaz
  - Volunteers
    - ankitv18
    - bbrala
    - lostcarpark
    - nexusnovaz
    - sheetal-wish
- Maintainers accepting changes
  - Dave Long
  - quietone
- Tags
  - vendor (1)
  - Novice (1)
  - Needs followup (1)
  - documentation (1)


### The set

    $ git log --oneline --reverse --since=2024-09-29T23:59:59+00:00 --until=2024-10-01T00:00:00+00:00
    4b7624537e Issue #3477373 by finnsky, bbrala, longwave: Fix "Not passing an instance of "TwigFunction" when creating a function of type "FunctionExpression" is deprecated."
    9ba6f74417 Issue #3462868 by annmarysruthy, yujiman85, nexusnovaz, pierregermain, ankitv18, sheetal-wish, smustgrave, lostcarpark, rodrigoaguilera, ultimike: Replace url usage of http://www.w3.org with https://www.w3.org in core

    $ git diff --shortstat 4b7624537e~1..9ba6f74417
     57 files changed, 108 insertions(+), 106 deletions(-)
