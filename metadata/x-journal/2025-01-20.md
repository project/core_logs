---
date: "2025-01-23"
title: "2025-01-20 (3 commits)"
---

## Overview

A fix on OOP hooks, a lint fix, and a documentation fix.

## Changes

It covers commits from 2025-01-20 on `11.x` branch.

### #3495586: PHPCS error in contributed module caused by core recipe.README.txt

[#3495586][3495586] - [d2e10cddc0b3bddd39f56b90092e81bd50ba41a6][d2e10cddc0b3bddd39f56b90092e81bd50ba41a6]

Fixes a phpcs lint.

[3495586]: https://www.drupal.org/project/drupal/issues/3495586
[d2e10cddc0b3bddd39f56b90092e81bd50ba41a6]: https://git.drupalcode.org/project/drupal/-/commit/d2e10cddc0b3bddd39f56b90092e81bd50ba41a6

### #3499263: Fix hook_install_tasks and hook_install_tasks_alter reference in HookCollectorPass

[#3499263][3499263] - [0be567305c82e0169af6be8b5d3cb4731b5b2b82][0be567305c82e0169af6be8b5d3cb4731b5b2b82]

Fixes a couple of hook names to deny from OOP hooks.

Follow up of 76323d6506ca2ec33fee0c30bceb425369e62596.

[3499263]: https://www.drupal.org/project/drupal/issues/3499263
[0be567305c82e0169af6be8b5d3cb4731b5b2b82]: https://git.drupalcode.org/project/drupal/-/commit/0be567305c82e0169af6be8b5d3cb4731b5b2b82

### #3495778: Update all references to file_save_htaccess()

[#3495778][3495778] - [798ad9e459020a5142a3b705fd95017e50179716][798ad9e459020a5142a3b705fd95017e50179716]

Replaces outdated references to `file_save_htaccess()` on the codebase.

[3495778]: https://www.drupal.org/project/drupal/issues/3495778
[798ad9e459020a5142a3b705fd95017e50179716]: https://git.drupalcode.org/project/drupal/-/commit/798ad9e459020a5142a3b705fd95017e50179716

## Annex

### Meta

- Commits: 3
- Approach:
  - Direct commit: 0
  - Merge request + Patch: 0
  - Merge request: 3
  - Patch: 0
- Shortest running issue: 7 days
- Longest running issue: 29 days
- People: 27
- Volunteers: 7
- Organizations: 18
  - Acquia
    - effulgentsia
    - gábor hojtsy
    - tedbow
    - tim.plunkett
  - Acro Commerce
    - alexpott
  - ActivIT s.r.o.
    - poker10
  - Aiven
    - webchick
  - DM13 Security LLC
    - cmlara
  - Drupal Association
    - drumm
  - Full Fat Things
    - longwave
  - HeroDevs
    - xjm
  - Jeneration Web Development
    - jenlampton
  - Kanopi Studios
    - thejimbirch
  - MD Systems GmbH
    - berdir
  - Mobomo
    - smustgrave
  - PreviousNext
    - kim.pepper
    - larowlan
    - quietone
  - Sopra Steria
    - bramdriesen
  - Tag1 Consulting
    - fabianx
  - Third and Grove
    - catch
  - Thunder
    - alexpott
  - Volunteers
    - benjifisher
    - bramdriesen
    - dawehner
    - mondrake
    - plach
    - quicksketch
    - wim leers
- Maintainers accepting changes
  - Alex Pott
  - quietone
- Parent issues

- Tags
  - strict (1)
  - Recipes initiative (1)
  - hooks (1)
  - dx (1)
  - documentation (1)


### The set

    $ git log --oneline --reverse --since=2025-01-19T23:59:59+00:00 --until=2025-01-21T00:00:00+00:00
    798ad9e4590 Issue #3495778 by bramdriesen, smustgrave, cmlara, dawehner, drumm, effulgentsia, fabianx, catch, berdir, alexpott, amateescu, BarisW, benjifisher, gábor hojtsy, guypaddock, tim.plunkett, webchick, wim leers, quicksketch, xjm, tedbow, poker10, kim.pepper, jenlampton, larowlan, longwave, plach, mondrake: Update all references to file_save_htaccess()
    0be567305c8 Issue #3499263 by nicxvan, smustgrave, berdir: Fix hook_install_tasks and hook_install_tasks_alter reference in HookCollectorPass
    d2e10cddc0b Issue #3495586 by tr, thejimbirch, quietone, cilefen, ghost of drupal past: PHPCS error in contributed module caused by core recipe.README.txt

    $ git diff --shortstat 798ad9e4590~1..d2e10cddc0b
     4 files changed, 8 insertions(+), 9 deletions(-)
