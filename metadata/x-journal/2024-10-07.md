---
date: "2024-10-09"
title: "2024-10-07 (11 commits)"
---

## Overview

Quite active and diverse day.

Changes range from improvement to database base classes, to recipes, to PHP 8.3
compatibility, to JavaScript.

## Changes

It covers commits from 2024-10-07 on `11.x` branch.

### #3478771: InputConfigurator should expose input data definitions

[#3478771][3478771] - [2ad947f2f3a4e16ae6080b2a8062f88f1568a782][2ad947f2f3a4e16ae6080b2a8062f88f1568a782]

Passes typed data on recipes `InputConfigurator`.

[3478771]: https://www.drupal.org/project/drupal/issues/3478771
[2ad947f2f3a4e16ae6080b2a8062f88f1568a782]: https://git.drupalcode.org/project/drupal/-/commit/2ad947f2f3a4e16ae6080b2a8062f88f1568a782

### #3479160: 11.0.x yarn dependencies have mushroomed

[#3479160][3479160] - [858610568b5e216fe87b77296c6d51c978adb37d][858610568b5e216fe87b77296c6d51c978adb37d]

Adds a test to verify the same CKEditor version is used in Node.js development
dependencies.

Apropos recent finding on 11.0.x that got one with a different version, and
ended up in 2GB of extra dependencies.

[3479160]: https://www.drupal.org/project/drupal/issues/3479160
[858610568b5e216fe87b77296c6d51c978adb37d]: https://git.drupalcode.org/project/drupal/-/commit/858610568b5e216fe87b77296c6d51c978adb37d

### #3478332: Add a way to prevent recipes' imported config from being compared too strictly to active config

[#3478332][3478332] - [f0fda63b583ca7d497c248026e47b2781fca026c][f0fda63b583ca7d497c248026e47b2781fca026c]

Introduces a way to opt into strict check on recipes.

Before, this was always a strict comparison; which naturally supported less
states.

[3478332]: https://www.drupal.org/project/drupal/issues/3478332
[f0fda63b583ca7d497c248026e47b2781fca026c]: https://git.drupalcode.org/project/drupal/-/commit/f0fda63b583ca7d497c248026e47b2781fca026c

### #3473014: Prefer to add "node:" prefix to require() which imports Node.js built-in module

[#3473014][3473014] - [dd72f0183babf14a2ae08c55e81ba2c25b64df0b][dd72f0183babf14a2ae08c55e81ba2c25b64df0b]

Use `node:` prefix on JavaScript `require()` calls to import built-in Node.js
module.

[3473014]: https://www.drupal.org/project/drupal/issues/3473014
[dd72f0183babf14a2ae08c55e81ba2c25b64df0b]: https://git.drupalcode.org/project/drupal/-/commit/dd72f0183babf14a2ae08c55e81ba2c25b64df0b

### #3359649: User routes alter in custom module throwing error on "\_format"

[#3359649][3359649] - [a6a40068166ae90c9c14ca43d83a91e93591e21b][a6a40068166ae90c9c14ca43d83a91e93591e21b]

Adds an extra check on `serialization` module to better handle routes with
`_format` requirement.

[3359649]: https://www.drupal.org/project/drupal/issues/3359649
[a6a40068166ae90c9c14ca43d83a91e93591e21b]: https://git.drupalcode.org/project/drupal/-/commit/a6a40068166ae90c9c14ca43d83a91e93591e21b

### #3310963: Attach correct image/webp header in .htaccess when mime module is enabled

[#3310963][3310963] - [30bea1ebb1a9d1801e435ce47a01f4ba7c2cef6c][30bea1ebb1a9d1801e435ce47a01f4ba7c2cef6c]

Adds relevant MIME type header for `webp` images.

[3310963]: https://www.drupal.org/project/drupal/issues/3310963
[30bea1ebb1a9d1801e435ce47a01f4ba7c2cef6c]: https://git.drupalcode.org/project/drupal/-/commit/30bea1ebb1a9d1801e435ce47a01f4ba7c2cef6c

### #3477366: Fix closures in tests for PHP 8.4

[#3477366][3477366] - [97775e78a586595a08428de44d6afc6e749496d8][97775e78a586595a08428de44d6afc6e749496d8]

Makes three test's closures compatible with PHP 8.4.

[3477366]: https://www.drupal.org/project/drupal/issues/3477366
[97775e78a586595a08428de44d6afc6e749496d8]: https://git.drupalcode.org/project/drupal/-/commit/97775e78a586595a08428de44d6afc6e749496d8

### #2005626: Implement \Drupal\Core\Database\Query\Update::arguments

[#2005626][2005626] - [ea261a9d1a58b736c08adf51f0c1306962acc396][ea261a9d1a58b736c08adf51f0c1306962acc396]

Implements `Drupal\Core\Database\Query\ConditionInterface::arguments()` on
`\Drupal\Core\Database\Query\Update`.

[2005626]: https://www.drupal.org/project/drupal/issues/2005626
[ea261a9d1a58b736c08adf51f0c1306962acc396]: https://git.drupalcode.org/project/drupal/-/commit/ea261a9d1a58b736c08adf51f0c1306962acc396

### #3100083: Add js message theme override to match Umami message markup

[#3100083][3100083] - [0e07179f7bd04a1fd333719852fee76799cd7efe][0e07179f7bd04a1fd333719852fee76799cd7efe]

Tweaks `umami` message styling.

[3100083]: https://www.drupal.org/project/drupal/issues/3100083
[0e07179f7bd04a1fd333719852fee76799cd7efe]: https://git.drupalcode.org/project/drupal/-/commit/0e07179f7bd04a1fd333719852fee76799cd7efe

### #3387960: Fix return type in \Drupal\Core\Database\Query\Merge::__toString

[#3387960][3387960] - [5f1bd5ffbdbf4056eab41a6fb9f455ed5fe20dd0][5f1bd5ffbdbf4056eab41a6fb9f455ed5fe20dd0]

Improves merge query string conversion.

It now trows an exception instead of not returning.

[3387960]: https://www.drupal.org/project/drupal/issues/3387960
[5f1bd5ffbdbf4056eab41a6fb9f455ed5fe20dd0]: https://git.drupalcode.org/project/drupal/-/commit/5f1bd5ffbdbf4056eab41a6fb9f455ed5fe20dd0

### #3474123: Reorganize navigation settings to be more consistent

[#3474123][3474123] - [d23a9f370698556991b6b9138a5eb1dd9e4c50d8][d23a9f370698556991b6b9138a5eb1dd9e4c50d8]

Tweaks `navigation` module configuration structure.

Make them multi-level instead of flat.

[3474123]: https://www.drupal.org/project/drupal/issues/3474123
[d23a9f370698556991b6b9138a5eb1dd9e4c50d8]: https://git.drupalcode.org/project/drupal/-/commit/d23a9f370698556991b6b9138a5eb1dd9e4c50d8

## Annex

### Meta

- Commits: 11
- Approach:
  - Direct commit: 0
  - Merge request + Patch: 4
  - Merge request: 7
  - Patch: 0
- Shortest running issue: 17 hours
- Longest running issue: 11 years
- People: 31
- Volunteers: 9
- Organizations: 30
  - Acquia
    - phenaproxima
    - zrpnr
  - Acro Commerce
    - alexpott
  - Annertech
    - markconroy
  - Axelerant
    - gauravvvv
  - Catalyst IT
    - gold
  - DROWL.de
    - anybody
  - Drupal Association
    - fjgarlin
  - Finalist
    - daffie
  - Full Fat Things
    - longwave
  - HeroDevs
    - xjm
  - Kanopi Studios
    - thejimbirch
  - Lullabot
    - plopesc
  - Material
    - arunkumark
  - Mobomo
    - smustgrave
  - OPTASY
    - xjm
  - OpenSense Labs
    - _utsavsharma
  - PreviousNext
    - mstrelan
    - quietone
  - QED42
    - shalini_jha
  - Reload
    - arnested
  - Seidor
    - rcodina
  - Skilld
    - andypost
    - finnsky
  - Specbee
    - ankithashetty
    - neslee canil pinto
  - The Confident
    - markconroy
  - The University of Arizona
    - joegraduate
  - Third and Grove
    - catch
  - Thunder
    - alexpott
  - Très Bien Tech
    - nod_
  - Valuebound
    - neelam_wadhwani
  - Zoocha
    - xjm
  - Volunteers
    - andypost
    - ankithashetty
    - arunkumark
    - fjgarlin
    - markconroy
    - mondrake
    - nod_
    - sukr_s
- Maintainers accepting changes
  - Alex Pott
  - Dave Long
  - catch
  - nod_
- Tags
  - Needs Review Queue Initiative (3)
  - configuration (3)
  - Starshot blocker (2)
  - Recipes initiative (2)
  - database (2)
  - Barcelona2024 (2)
  - vendor (1)
  - theme (1)
  - tests (1)
  - styling (1)
  - server (1)
  - routing (1)
  - PHP 8.4 (1)
  - performance budget change (1)
  - needs backport to 10.x (1)
  - JavaScript (1)
  - javascript (1)
  - Coding standards (1)
  - release notes (0)


### The set

    $ git log --oneline --reverse --since=2024-10-06T23:59:59+00:00 --until=2024-10-08T00:00:00+00:00
    d23a9f3706 Issue #3474123 by plopesc, smustgrave, alexpott: Reorganize navigation settings to be more consistent
    5f1bd5ffbd Issue #3387960 by quietone, alexpott, daffie, mondrake, amateescu: Fix return type in \Drupal\Core\Database\Query\Merge::__toString
    0e07179f7b Issue #3100083 by finnsky, Zsuffa Dávid, ankithashetty, _utsavsharma, gauravvvv, smustgrave, zrpnr, markconroy: Add js message theme override to match Umami message markup
    ea261a9d1a Issue #2005626 by sukr_s, gold, jhedstrom, neelam_wadhwani, neslee canil pinto, jweowu, Mixologic, quietone, daffie, smustgrave, longwave, mondrake: Implement \Drupal\Core\Database\Query\Update::arguments
    97775e78a5 Issue #3477366 by andypost: Fix closures in tests for PHP 8.4
    30bea1ebb1 Issue #3310963 by rcodina, quietone, anybody, joegraduate, smustgrave, xjm, cilefen, alexpott, larowlan: Attach correct image/webp header in .htaccess when mime module is enabled
    a6a4006816 Issue #3359649 by arnested, shalini_jha, alexpott, arunkumark, cilefen, smustgrave, aduthois, catch, quietone: User routes alter in custom module throwing error on "_format"
    dd72f0183b Issue #3473014 by tom konda: Prefer to add "node:" prefix to require() which imports Node.js built-in module
    f0fda63b58 Issue #3478332 by phenaproxima, nicxvan, thejimbirch, alexpott: Add a way to prevent recipes' imported config from being compared too strictly to active config
    858610568b Issue #3479160 by longwave, catch, fjgarlin, nod_: 11.0.x yarn dependencies have mushroomed
    2ad947f2f3 Issue #3478771 by phenaproxima, smustgrave, alexpott: InputConfigurator should expose input data definitions

    $ git diff --shortstat d23a9f3706~1..2ad947f2f3
     60 files changed, 815 insertions(+), 346 deletions(-)
