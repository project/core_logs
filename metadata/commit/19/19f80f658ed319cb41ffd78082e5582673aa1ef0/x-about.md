Improves cache related headers in HTTP responses to better indicate what is
happening.

Nice summary of the change on the [related change
record][change-record-2958442].

[change-record-2958442]: https://www.drupal.org/node/2958442
