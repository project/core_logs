Changes how dblog filter internal logic works to use condition objects.

Improves MongoDB compatibility.
