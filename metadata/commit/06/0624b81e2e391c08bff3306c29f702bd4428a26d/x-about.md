Fixes artifact download caching on CI.

A variable provided by gitlab was different depending on the author of the git
push, which ends up in the process skipping cache.
That is now fixed!
