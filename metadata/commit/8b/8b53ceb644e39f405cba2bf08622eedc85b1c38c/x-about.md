Tweaks the `UpdatePathTestBase` to skip a entity save and instead use direct
database changes.
