Use `ConstraintViolationInterface` instead of `ConstraintViolation` to better
follow the actually returned values in `ckeditor5` module tests.
