Improves libraries asset ordering based on library dependencies, especially for
the aggregated case.
