Adds `FilterArray::removeEmptyStrings()` to replace `array_filter()` calls where
callable is expecting a bool return.
