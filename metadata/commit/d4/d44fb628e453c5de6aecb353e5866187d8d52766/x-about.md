Makes big pipe more resilient to an edge case making its use of `loadjs`
JavaScript library more error-proof.
