Makes views export cleaner for the remember_roles setting.

Nice, especially on sites that change roles, this tends to make non-idempotent
exports.
