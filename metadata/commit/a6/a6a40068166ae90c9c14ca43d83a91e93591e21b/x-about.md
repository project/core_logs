Adds an extra check on `serialization` module to better handle routes with
`_format` requirement.
