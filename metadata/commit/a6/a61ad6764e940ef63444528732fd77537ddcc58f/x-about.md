Adds extra validation for gettext `po` files to ensure its encoding is UTF-8.

Adds a new related constraint validator that check file encoding.
