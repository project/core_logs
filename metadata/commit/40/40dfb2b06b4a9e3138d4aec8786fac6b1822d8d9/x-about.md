Tweaks `drupal.tabledrag` JavaScript to use `window.scrollY` instead of
`window.pageYOffset`, which is an alias of the former.
