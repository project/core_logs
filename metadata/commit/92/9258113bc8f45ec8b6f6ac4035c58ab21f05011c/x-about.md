Follow up fix for related main commit [bf4ae81][bf4ae811643c6e50e5263e19f8eb28e123e4d855].

[bf4ae811643c6e50e5263e19f8eb28e123e4d855]: https://git.drupalcode.org/project/drupal/-/commit/bf4ae811643c6e50e5263e19f8eb28e123e4d855
