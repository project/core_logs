Fixes a bug while publishing on `content_moderation` and `workflows`
integration.
