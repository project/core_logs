Enables `phpcs`' `Drupal.Commenting.ClassComment.Missing` in test modules and
fixes remaining non-compliant cases.
