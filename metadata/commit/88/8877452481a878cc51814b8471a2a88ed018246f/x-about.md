Reduces CPU requirements of gitlab CI workers.

Tweaks knobs around parallelism, concurrency, and minimal CPU requirements,
around test execution.
