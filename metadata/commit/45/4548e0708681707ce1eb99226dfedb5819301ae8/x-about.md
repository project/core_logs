Tweaks `phpstan` configuration to detect missing return types for any new code,
and ignore the existing code that is not passing for now.
