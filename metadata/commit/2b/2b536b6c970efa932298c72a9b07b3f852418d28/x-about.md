Clean revert of git commit [c1f7b85decd7b1281b00d891c72963551b57d622][c1f7b85decd7b1281b00d891c72963551b57d622] around a workspaces UI fix.

Improved fix with extra test coverage currently in review.

[c1f7b85decd7b1281b00d891c72963551b57d622]: https://git.drupalcode.org/project/drupal/-/commit/c1f7b85decd7b1281b00d891c72963551b57d622
