Prevents scroll on breadcrumbs when using `olivero` theme when breadcrumbs
content is long.
