Adds a test to verify the same CKEditor version is used in Node.js development
dependencies.

Apropos recent finding on 11.0.x that got one with a different version, and
ended up in 2GB of extra dependencies.
