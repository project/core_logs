Improves `AssertContentTrait::setRawContent()` documentation, to allow phpstan
to know a `\Stringable` can also be used.
