Converted a few static database queries into dynamic ones to better support
non-SQL databases like MongoDB.
