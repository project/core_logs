Expands `jsonapi` module to allow re-enabling a disabled resource type field via
the new `ResourceTypeBuildEvent::enableField()` method.
