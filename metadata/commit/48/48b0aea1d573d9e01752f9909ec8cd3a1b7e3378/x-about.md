Improves error handling to prevent possible sensitive information to be
presented on the error message.
