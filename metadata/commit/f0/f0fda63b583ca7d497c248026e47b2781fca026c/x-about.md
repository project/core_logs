Introduces a way to opt into strict check on recipes.

Before, this was always a strict comparison; which naturally supported less
states.
