Improves usage of `DateTimePlus::createFromFormat()` to be more strict around
the type of passed parameters.
