Improved access restriction support for rendering entity links with
`Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter`.

Now links will only be shown if the current user has access to the linked
entity, instead of showing it regardless.
