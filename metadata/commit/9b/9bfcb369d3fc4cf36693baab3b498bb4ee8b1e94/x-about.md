Fixes a `jsonapi` normalization problem caused by cache being set at incorrect
values.

Quite a puzzle that has been solved here 👍
