#!/bin/sh

export LANG=C

generate_index() {
    tee <<START
# Core Logs

<img src="./core-logs-druplicon.svg" width="100px" height="100px" style="float: right" alt="core logs druplicon logo" />

[About][about]

## Journal

A journal of changes in core by day.

Recent journal entries.

START

    EXTRA_FILE=$(mktemp)
    FOOTER_FILE=$(mktemp)
    LIST_SHAPE_FILE=$(mktemp)
    ls metadata/x-journal/*.md | sort -r | while read JOURNAL_FILE
    do
        I=${I:-0}
        JOURNAL_FILE_NAME=$(basename "$JOURNAL_FILE")
        DAY=${JOURNAL_FILE_NAME%.md}
        TITLE=$(head "$JOURNAL_FILE" | grep ^title: | sed -e 's/title: "\(.*\)"/\1/')
        if [ $I -lt 7 ]
        then
            echo "- [$TITLE][$DAY]"
        else
            echo 'full' > "$LIST_SHAPE_FILE"
            echo "- [$TITLE][$DAY]" >> "$EXTRA_FILE"
        fi
        echo "[$DAY]: /core_logs/journal/${DAY}.html" >> $FOOTER_FILE
        I=$(( $I + 1 ))
    done
    echo ''
    if [ ! -z "$(cat "$LIST_SHAPE_FILE")" ]
    then
        tee <<JOURNAL_ENTRIES
<details><summary>Older journal entries</summary>
$(cat $EXTRA_FILE)
</details>
$(cat "$FOOTER_FILE")
JOURNAL_ENTRIES
    fi
    echo '[Journal RSS][journal-rss]'
    echo ''
    cat "$FOOTER_FILE"
    rm -f "$EXTRA_FILE" "$FOOTER_FILE" "$LIST_SHAPE_FILE"

    tee <<CHANGES
## Changes

<details>
<summary>Interval reviews, no longer active, but it may come back in the future.</summary>

[Changes RSS][changes-rss]

CHANGES

    LINKS_FILE=$(mktemp)
    ls changes/2*.md | while read CHANGES_FILE
    do
        DATE=$(head "$CHANGES_FILE" | grep ^date: | sed -e 's/date: "\(.*\)"/\1/')
        TITLE=$(head "$CHANGES_FILE" | grep ^title: | sed -e 's/title: "\(.*\)"/\1/')
        echo "- [$TITLE][$DATE]"
        echo "[$DATE]: /core_logs/changes/$DATE.html" >> $LINKS_FILE
    done
    echo ''
    cat "$LINKS_FILE"
    rm -f "$LINKS_FILE"

    tee <<END
</details>
[about]: /core_logs/about.html
[changes-rss]: /core_logs/changes-rss.xml
[journal-rss]: /core_logs/journal-rss.xml
END
}

ensure_pandoc_rss() {
    if [ ! -d pandoc-rss-1.0.1 ]
    then
        curl -L https://github.com/chambln/pandoc-rss/archive/refs/tags/1.0.1.tar.gz | tar zxf -
    fi
}

generate_rss_changes() {
    ensure_pandoc_rss
    ./pandoc-rss-1.0.1/bin/pandoc-rss \
        -o \
        -s \
        -d 'Core Logs points out some information around Drupal core development, to follow what is happening there over time. This feed is about custom commit interval set.' \
        -f 'changes/%s.html' \
        -l 'https://project.pages.drupalcode.org/core_logs/' \
        -n en-US \
        -t 'Core Logs: Changes' \
        -w 'https://www.drupal.org/u/marvil07' \
        changes/2*.md
}

generate_rss_journal() {
    ensure_pandoc_rss
    ./pandoc-rss-1.0.1/bin/pandoc-rss \
        -s \
        -d 'Core Logs points out some information around Drupal core development, to follow what is happening there over time. This feed is an entry per day, for up to one week.' \
        -f 'journal/%s.html' \
        -l 'https://project.pages.drupalcode.org/core_logs/' \
        -n en-US \
        -t 'Core Logs: Journal' \
        -w 'https://www.drupal.org/u/marvil07' \
        $(ls metadata/x-journal/*.md | sort -r | head -n 7 | xargs)
}

mkdir -p public/changes public/journal
generate_index > index.md
pandoc -s -o public/about.html about.md
pandoc -s -o public/index.html index.md
ls changes/2*.md | while read CHANGES_FILE
do
    pandoc -s -o "public/${CHANGES_FILE%md}html" "$CHANGES_FILE"
done
ls metadata/x-journal/*.md | while read JOURNAL_FILE
do
    JOURNAL_FILE_NAME=$(basename "$JOURNAL_FILE")
    pandoc -s -o "public/journal/${JOURNAL_FILE_NAME%md}html" "$JOURNAL_FILE"
done
generate_rss_changes > public/changes-rss.xml
generate_rss_journal > public/journal-rss.xml
if [ ! -f public/core-logs-druplicon.svg ]
then
    cp core-logs-druplicon.svg public/
fi
