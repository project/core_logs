# About

Core Logs points out some information around Drupal core development, to follow
what is happening there over time.
It intends to help communicate about those source code changes.

It is heavily inspired by [LWN][lwn]'s *Merge window* and *Development
statistics* reports around the Linux Kernel.
E.g. see [The first half of the 6.3 merge window][linux-6-3-merge] and
[Development statistics for 6.3][linux-6-3-stats].

The canonical resources to find out what has happened in a new version is
naturally the combination of the official release notes and related change
record entries.

Drupal core release notes are just part of the release page, e.g.
[10.0.0 release notes][10.0.0] are a really good reference on the changes.

[Drupal core change records][core-change-records] are intended to document
details around specific changes, e.g. [New starterkit theme
generator][starterkit-change-record].
Even if not every commit/issue have one, when the change is introducing
something that needs extra clarification, there is usually one available.

[10.0.0]: https://www.drupal.org/project/drupal/releases/10.0.0
[core-change-records]: https://www.drupal.org/list-changes/drupal
[linux-6-3-merge]: https://lwn.net/Articles/923846/
[linux-6-3-stats]: https://lwn.net/Articles/929582/
[lwn]: https://lwn.net
[starterkit-change-record]: https://www.drupal.org/node/3206389
