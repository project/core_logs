#!/bin/sh

EXISTING_ENTRIES_FILE=$(mktemp)
EXPECTED_ENTRIES_FILE=$(mktemp)

ls ./metadata/x-journal/ | sed -e 's/\(.*\)\.md/\1/' > "$EXISTING_ENTRIES_FILE"

# Ideally let us close the gap starting on this date.
START_DAY='2024-09-11'
TODAY="$(date +%Y-%m-%d)"
DAY="$START_DAY"
echo '' > "$EXPECTED_ENTRIES_FILE"
while [ "$DAY" != "$TODAY" ]
do
    echo "$DAY" >> "$EXPECTED_ENTRIES_FILE"
    DAY="$(date -d "$DAY next day" +%Y-%m-%d)"
done
echo "Missing journals"
comm -3 --total "$EXISTING_ENTRIES_FILE" "$EXPECTED_ENTRIES_FILE"

rm -f "$EXISTING_ENTRIES_FILE" "$EXPECTED_ENTRIES_FILE"
