#!/bin/sh

# $1: seconds interval
seconds_to_readable() {
    TIMEDIFF="$1"
    TIMEDIFF_HOURS=$(( $TIMEDIFF / 3600 ))
    TIMEDIFF_DAYS=$(( $TIMEDIFF_HOURS / 24 ))
    TIMEDIFF_MONTHS=$(( $TIMEDIFF_DAYS / 30 ))
    TIMEDIFF_YEARS=$(( $TIMEDIFF_DAYS / 365 ))
    if [ "$TIMEDIFF_YEARS" -gt 0 ]
    then
        echo "$TIMEDIFF_YEARS years"
    elif [ "$TIMEDIFF_MONTHS" -gt 0 ]
    then
        echo "$TIMEDIFF_MONTHS months"
    elif [ "$TIMEDIFF_DAYS" -gt 0 ]
    then
        echo "$TIMEDIFF_DAYS days"
    else
        echo "$TIMEDIFF_HOURS hours"
    fi
}

START="$1"
END="$2"
INTERVAL="$START..$END"

TODAY=$(date +%Y-%m-%d)
COMMITS_N=$(git log --oneline $INTERVAL | wc -l)

tee <<INTRO
---
date: "$TODAY"
title: "$TODAY: FIXME Title"
---

FIXME General note.

## Changes

It covers commits added over the \`$INTERVAL\` interval on \`11.x\` branch.

INTRO

CONTRIBUTORS_FILE=$(mktemp)
INTERVAL_STATS_FILE=$(mktemp)
git log --no-decorate --oneline --reverse $INTERVAL | while read LINE
do
    HASH=$(echo $LINE | cut -f1 -d ' ')
    SUBJECT=$(echo $LINE | cut -f2- -d ' ')
    ISSUE=$(echo $SUBJECT | sed -e 's/.*#\([0-9]*\).*/\1/')
    CONTRIBUTORS=$(drupalorg issue credit "$ISSUE")
    echo "$CONTRIBUTORS" >> "$CONTRIBUTORS_FILE"
    SHORT_SUBJECT=$(echo $SUBJECT | sed -e 's/.*: //')
    COLONS_N=$(echo $LINE | grep -o : | wc -l)
    if [ "$COLONS_N" -gt 1 ]
    then
        # Unreliable regex, fix manually.
        SHORT_SUBJECT="FIXME $SUBJECT"
    fi
    tee <<EOT
### #$ISSUE: $SHORT_SUBJECT

[#$ISSUE][$ISSUE]

FIXME Description

[$ISSUE]: https://www.drupal.org/project/drupal/issues/$ISSUE

EOT
    FILES_N=$(drupalorg issue file_count "$ISSUE")
    MR_N=$(drupalorg issue mr_count "$ISSUE")
    LIFESPAN=$(drupalorg issue lifespan "$ISSUE")
    if [ "$FILES_N" -gt 0 -a "$MR_N" -gt 0 ]
    then
        APPROACH='both'
    else
        if [ "$FILES_N" -gt 0 ]
        then
            APPROACH='patch'
        elif [ "$MR_N" -gt 0 ]
        then
            APPROACH='mr'
        else
            # No files nor MRs, either direct commit or an error.
            APPROACH='unknown'
        fi
    fi
    echo "$ISSUE,$APPROACH,$LIFESPAN" >> $INTERVAL_STATS_FILE
done

CONTRIBUTORS_N=$(cat "$CONTRIBUTORS_FILE" | grep -v ^$ | cut -d"," -f1 | sort | uniq | wc -l)
ORGANIZATIONS_N=$(cat "$CONTRIBUTORS_FILE" | cut -d"," -f2 | sort | uniq | grep -v -e ^$ | sed -e "s/^ //" | wc -l)
CONTRIBUTORS_CSV=$(cat "$CONTRIBUTORS_FILE" | grep -v ^$ | sort | uniq)
CONTRIBUTOR_VOLUNTEERS_N=$(echo "$CONTRIBUTORS_CSV" | grep 1$ | wc -l)
CONTRIBUTORS_LIST=$(echo "$CONTRIBUTORS_CSV" | php -r '
  $contributors_by_org = [];
  $volunteers = [];
  while ($line = trim(fgets(STDIN))) {
    $contributor = str_getcsv($line);
    $handle = trim($contributor[0]);
    $organization = trim($contributor[1]);
    $contributors_by_org[$organization][$handle] = 1;
    $is_volunteer = (bool) trim($contributor[2]);
    if ($is_volunteer) {
      $contributors_by_org["Volunteers"][$handle] = 1;
    }
  }
  ksort($contributors_by_org);
  foreach ($contributors_by_org as $organization => $org_contributors) {
    echo "  - $organization", PHP_EOL;
    #sort($org_contributors);
    $org_contributors = array_keys($org_contributors);
    foreach ($org_contributors as $org_contributor) {
      echo "    - $org_contributor", PHP_EOL;
    }
  }
')
rm -f "$CONTRIBUTORS_FILE"

APPROACH_PATCH=$(grep patch "$INTERVAL_STATS_FILE" | wc -l)
APPROACH_MR=$(grep mr "$INTERVAL_STATS_FILE" | wc -l)
APPROACH_BOTH=$(grep both "$INTERVAL_STATS_FILE" | wc -l)
APPROACH_DIRECT=$(grep unknown "$INTERVAL_STATS_FILE" | wc -l)
LIFESPAN_MIN=$(cat "$INTERVAL_STATS_FILE" | cut -d "," -f 3 | grep -v ^$ | sort -n | head -n 1)
LIFESPAN_MIN_READABLE=$(seconds_to_readable "$LIFESPAN_MIN")
LIFESPAN_MAX=$(cat "$INTERVAL_STATS_FILE" | cut -d "," -f 3 | grep -v ^$ | sort -n -r | head -n 1)
LIFESPAN_MAX_READABLE=$(seconds_to_readable "$LIFESPAN_MAX")
rm -f "$INTERVAL_STATS_FILE"

START_TS=$(git log --format="%ad" --date=unix -1 $START)
END_TS=$(git log --format="%ad" --date=unix -1 $END)
INTERVAL_DAYS=$(( ( $END_TS - $START_TS ) / (3600*24) ))

MAINTAINERS_LIST=$(git shortlog --summary --committer $INTERVAL | cut -f 2 | sed -e 's/^/  - /')

tee <<OUTRO
## Closing

FIXME Description

## Annex

### Meta

- Commits: $COMMITS_N
- Approach:
  - Direct commit: $APPROACH_DIRECT
  - Merge request + Patch: $APPROACH_BOTH
  - Merge request: $APPROACH_MR
  - Patch: $APPROACH_PATCH
- Days: $INTERVAL_DAYS
- Shortest running issue: $LIFESPAN_MIN_READABLE
- Longest running issue: $LIFESPAN_MAX_READABLE
- People: $CONTRIBUTORS_N
- Volunteers: $CONTRIBUTOR_VOLUNTEERS_N
- Organizations: $ORGANIZATIONS_N
$CONTRIBUTORS_LIST
- Maintainers accepting changes
$MAINTAINERS_LIST

### The set

FIXME indent
    $ git log --oneline --reverse $INTERVAL
$(git log --oneline --reverse $INTERVAL)

    $ git diff --shortstat $INTERVAL
$(git diff --shortstat $INTERVAL)

    $ git shortlog --summary --committer $INTERVAL | sort -n -r
$(git shortlog --summary --committer $INTERVAL | sort -n -r)
OUTRO
