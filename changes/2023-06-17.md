---
date: "2023-06-17"
title: "2023-06-17: Quite active 5 days"
---

This is quite a big recap :sweat_smile:

## Changes

It covers commits added over the `23fc2b6c35..570710ad7b` interval on `11.x` branch.

### #3221128: Enable service autoconfiguration for core event subscribers

[#3221128][3221128]

Drupal and Symfony magic working together to improve developer experience and
less lines to add on service definitions.
More information on the [change record][3357408].

An extract below!

```
    --- a/core/core.services.yml
    +++ b/core/core.services.yml
    @@ -60,6 +60,8 @@ parameters:
       queue.config:
         suspendMaximumWait: 30.0
     services:
    +  _defaults:
    +    autoconfigure: true
       # Simple cache contexts, directly derived from the request context.
       cache_context.ip:
         class: Drupal\Core\Cache\Context\IpCacheContext
    
    --- a/core/lib/Drupal/Core/CoreServiceProvider.php
    +++ b/core/lib/Drupal/Core/CoreServiceProvider.php
    @@ -97,6 +98,8 @@ public function register(ContainerBuilder $container) {
    
         $container->addCompilerPass(new DeprecatedServicePass());
    
    +    $container->registerForAutoconfiguration(EventSubscriberInterface::class)
    +      ->addTag('event_subscriber');
       }
    
       /**
    --- a/core/lib/Drupal/Core/DependencyInjection/YamlFileLoader.php
    +++ b/core/lib/Drupal/Core/DependencyInjection/YamlFileLoader.php
    @@ -36,6 +36,7 @@ class YamlFileLoader
             'public' => 'public',
             'tags' => 'tags',
             'autowire' => 'autowire',
    +        'autoconfigure' => 'autoconfigure',
         ];
     
         /**
    @@ -254,6 +255,9 @@ private function parseDefinition(string $id, $service, string $file, array $defa
             if (isset($defaults['autowire'])) {
                 $definition->setAutowired($defaults['autowire']);
             }
    +        if (isset($defaults['autoconfigure'])) {
    +            $definition->setAutoconfigured($defaults['autoconfigure']);
    +        }
     
             $definition->setChanges([]);
```
     
[3221128]: https://www.drupal.org/project/drupal/issues/3221128
[3357408]: https://www.drupal.org/node/3357408

### #3365480: [SDC] Improve error handling during prop validation errors

[#3365480][3365480]

More love for single directory components, this time about YAML parsing.

[3365480]: https://www.drupal.org/project/drupal/issues/3365480

### #3332456: [random test failure] MediaTest::testLinkManualDecorator()

[#3332456][3332456]

Random test failures are just painful when you run into them.
This change work-around most of the problem with the changed test, but as
mentioned on the issue it may be an actual upstream bug.

[3332456]: https://www.drupal.org/project/drupal/issues/3332456

### #3366287: [regression] Inserting media via the media library modal when paged redirects to the wrong destination

[#3366287][3366287]

One less regression \o/ on media library.
These kind changes impact editors positively, and make them happy :+1:

[3366287]: https://www.drupal.org/project/drupal/issues/3366287

### #3348093: Deprecate user_roles() and user_role_names()

[#3348093][3348093]

A decade over a codebase is not bad `user_role_names()`!
And `user_roles()`, it has been 22 years for you!
And even some more with BC compatibility period.

A couple of new deprecated functions, and interestingly its use has been mainly
replaced with a more functional approach.

[3348093]: https://www.drupal.org/project/drupal/issues/3348093

### #3366314: Remove composer optimisations for laminas/laminas-zendframework-bridge

[#3366314][3366314]

Clean up removing last remains of laminas zend framework bridge dependency,
removed a couple of years ago, at [#3238201][3238201].

[3366314]: https://www.drupal.org/project/drupal/issues/3366314
[3238201]: https://www.drupal.org/project/drupal/issues/3238201

### #3366633: Uncomment assertions in StyleTest related to https://github.com/ckeditor/ckeditor5/issues/11709

[#3366633][3366633]

Depending on projects like ckeditor is great to get a lot of functionality, and
from time to time there may be some glitches.
This is one of those, that was fixed now upstream, and related test cases could
now be re-enabled.

[3366633]: https://www.drupal.org/project/drupal/issues/3366633

### #3270647: PhpMail : broken mail headers in PHP 8.0+ because of LF characters

[#3270647][3270647]

It may be that line ending variations among different operating systems will
never stop needing to have some special attention.

The good thing is that this time, at least on the Drupal side, a work-around is
being removed, following a change on PHP, around line endings for email headers.

The [actual change on PHP is nicely small][php-47983], and it is about following
the actual mail standard.
And that php commit message conveys clearly the message :-)

> Fix #47983: mixed LF and CRLF line endings in mail()
> 
> Email headers are supposed to be separated with CRLF.  Period.

[3270647]: https://www.drupal.org/project/drupal/issues/3270647
[php-47983]: https://github.com/php/php-src/commit/6983ae751cd301886c966b84367fc7aaa1273b2d

### #3319184: hook_condition_info_alter is not documented

[#3319184][3319184]

Added documentation for a hook added as part of the [original condition plugin
system][plugin-system-init] :+1:

[3319184]: https://www.drupal.org/project/drupal/issues/3319184
[plugin-system-init]: https://git.drupalcode.org/project/drupal/-/commit/89686bf45cf5a15fbbbf79c5d05724f78468e3b1#a259a33bd90d54d2aa9e19556fdcc3487f414d3e_0_30

### #3027705: Allow ?edit[field_xyz] as query parameter in contextual filter

[#3027705][3027705]

More power on query parameters for views, now nested values are OK.

The change is nicely clean, using `NestedArray` to handle the main change.

[3027705]: https://www.drupal.org/project/drupal/issues/3027705

### #3347873: Centralize permission checks in a service

[#3347873][3347873]

Quite nice API addition, per original issue description, it helps to override
the access in a more natural way, that was living in two different places, by
adding a decorator.

This is also a nice developer experience improvement, thanks!

[3347873]: https://www.drupal.org/project/drupal/issues/3347873

### #3346560: Allow extending StatusMessages class

[#3346560][3346560]

This is a nice developer experience improvement change too.
If you ever wanted to override status messages, this may simplify your code.

[3346560]: https://www.drupal.org/project/drupal/issues/3346560

### #3366288: Add [#ReturnTypeWillChange] attribute to TemporaryArrayObjectThrowingExceptions for PHP 8.3 compatibility

[#3366288][3366288]

Makes a Drupal a bit more compatible with 8.2.

The related [PHP RFC: Add true type][php-rfc-true-type] is an interesting read.
More consistency to the PHP language is still being added.

[3366288]: https://www.drupal.org/project/drupal/issues/3366288

### #3325557: Enable more service autowiring by adding interface aliases to core modules

[#3325557][3325557]

More [symfony auto-wiring][symfony-autowiring] support.
The related follow-up at [#3295751][3295751] has a [set of changes that exemplify how this could help][more-autowire].

[3325557]: https://www.drupal.org/project/drupal/issues/3325557
[symfony-autowiring]: https://symfony.com/doc/current/service_container/autowiring.html
[more-autowire]: https://git.drupalcode.org/project/drupal/-/merge_requests/2508/diffs#83f82e67ea39b425b6a5e29e118177a2d39059c1
[3295751]: https://www.drupal.org/project/drupal/issues/3295751

### #3366081: [random test failure] MediaTest:: testEditableCaption()

[#3366081][3366081]

One less random test failure to worry about, so grateful to see those tackled.
They impact everyone relying on testbot results.

[3366081]: https://www.drupal.org/project/drupal/issues/3366081

### #3355122: ContentTranslationContextualLinksTest should use API calls to set up translation

[#3355122][3355122]

Polishing existing test, and making it more efficient.
Every improvement there reflects in the future test runs with lower resources
and time to execute the set :+1:

[3355122]: https://www.drupal.org/project/drupal/issues/3355122

### #3366481: OEmbedIframeController returns an HTTP response code that can be cached by forward proxies when it is given illegal parameters

[#3366481][3366481]

Improved forward proxy support on media oembed.
Interestingly originally reported as security issue, possibly for a likely DoS
for valid oembeds.

[3366481]: https://www.drupal.org/project/drupal/issues/3366481

### #3346765: Add PerformanceTestBase for allowing browser performance assertions within FunctionalJavaScriptTests

[#3346765][3346765]

This is quite interesting, the initial added changes on the issue were a bit
more general, and later the scope was reduced, but it adds the basis for
performance testing in core!

For now, it collects number of CSS and JavaScript files loaded, but via chrome
through mink, but more are planned on different issues.
A good starting point to look around this topic may be [#638078][638078].

[3346765]: https://www.drupal.org/project/drupal/issues/3346765
[638078]: https://www.drupal.org/project/drupal/issues/638078

### #3309585: CKEditor 5 Style plugin configuration tab does not appear

[#3309585][3309585]

Bug fix in ckeditor style plugin :+1:

[3309585]: https://www.drupal.org/project/drupal/issues/3309585

### #3334489: ChainedFastBackend invalidates all items when cache tags are invalidated

[#3334489][3334489]

Bug fix to avoid clearing unneeded cache bins for this cache back-end.
Drupal getting faster here :+1:

[3334489]: https://www.drupal.org/project/drupal/issues/3334489

### #3052115: Mark an entity as 'syncing' during a migration update

[#3052115][3052115]

This is part of a series to make migration import updates more accurate, by not
changing the changed time stamp on entities.
Temporally reverted and then re-added with an extra fix.
More information on related issues.
Consistency++

[3052115]: https://www.drupal.org/project/drupal/issues/3052115

### #3275557: Add webp image conversion to core's install profile's image style

[#3275557][3275557]

PHP GD library has support for WebP since `7.0.10`.
Drupal core `gd` ImageToolkit plugin using that library support WebP since
d829a0e5c6edae7fc13438a39e44ca0915b8677e.

This change adds default configuration on the standard profile to by default
allow webp extension on multiple places.

[3275557]: https://www.drupal.org/project/drupal/issues/3275557

### #3366722: Add Lauri Eskola to Drupal core product managers

[#3366722][3366722]

Lauri has transitioned from being a Drupal Core Frontend Framework Manager to a Product Manager now.
Congrats!

[3366722]: https://www.drupal.org/project/drupal/issues/3366722

### #3256642: Introduce database driver extensions and autoload database drivers' dependencies

[#3256642][3256642]

Drupal has extensions that can be modules, profiles, and themes.
This change converts the database drivers into another possible extension.

This change seems to unify more the approach, reducing special cases to handle
databases, that sounds great!

More information on the [related change record][3258175]

[3256642]: https://www.drupal.org/project/drupal/issues/3256642
[3258175]: https://www.drupal.org/node/3258175

### #3355403: Add "Edit permisisons" as local task on role edit form

[#3355403][3355403]

Quite nice administration improvement, a new link to find permissions on roles.

[3355403]: https://www.drupal.org/project/drupal/issues/3355403

### #3129669: Block content set contextual edit links to edit_form route instead of canonical

[#3129669][3129669]

Consistency fix on `block_content.block_edit` contextual link.

[3129669]: https://www.drupal.org/project/drupal/issues/3129669


### #3359240: Enable bundle selection when a new view mode is created

[#3359240][3359240]

An administration improvement around view modes UI, smoother site builder
experience as a result :+1:

[3359240]: https://www.drupal.org/project/drupal/issues/3359240

### #3356515: Give users a way to access announcements if toolbar module is disabled

[#3356515][3356515]

Improvement for the recently added announcements experimental module, so there
is a route outside the toolbar.

[3356515]: https://www.drupal.org/project/drupal/issues/3356515

### #3351886: Change SchemaUniquePrefixedKeysIndexTest to extend DriverSpecificDatabaseTestBase

[#3351886][3351886]

Improvement over tests, for a more consistent approach.

[3351886]: https://www.drupal.org/project/drupal/issues/3351886

### #3261574: Add .engine to files scanned by PHPCS

[#3261574][3261574]

PHPCS scanning, for the one engine file in core around twig.

[3261574]: https://www.drupal.org/project/drupal/issues/3261574

### #3360139: Phpdoc for ResourceTypeRepositoryInterface::get return value is incomplete

[#3360139][3360139]

DX improvement via documentation fix :+1:

[3360139]: https://www.drupal.org/project/drupal/issues/3360139

### #3135457: Remove outdated @todo's pointing to #3135457

[#3361465][3361465]

Removes documentation pointing to no longer relevant issue.

[3361465]: https://www.drupal.org/project/drupal/issues/3361465

### #2662330: Machine name generation is way too slow

[#2662330][2662330]

Another nice administration UX improvement!
I have hit this problem when creating content models, and it is sooo nice to see
it addressed, thanks!

New JavaScript dependencies are added in the process, so the machine generated
name is generated asynchronously and using the transliteration library.

[2662330]: https://www.drupal.org/project/drupal/issues/2662330

## Closing

This has been quite a big set, and I am glad to see so much and nice activity on
core.
It just took a couple of iterations to finish the notes :sweat_smile:

## Annex

### Meta

- Commits: 35
- Approach:
  - Direct commit: 0
  - Merge request + Patch: 0
  - Merge request: 0
  - Patch: 0
- Days: 5
- Shortest running issue: 3 hours
- Longest running issue: 7 years
- People: 93
- Volunteers: 24
- Organizations: 45
  - Acquia
    - bnjmnm
    - hooroomoo
    - lauriii
    - mglaman
    - narendraR
    - phenaproxima
    - tim.plunkett
    - Utkarsh_33
    - Wim Leers
  - Affinity Bridge
    - floydm
  - Agileana
    - xjm
  - Cheppers
    - huzooka
  - Acro Media Inc, Thunder
    - alexpott
  - ActivIT s.r.o
    - poker10
  - Big Island Fish, Not Vanilla Inc
    - bradjones1
  - Bluespark
    - jwilson3
  - Calibrate
    - borisson_
  - CivicActions
    - jastraat
  - CI&T
    - elber
    - murilohp
    - tmaiochi
  - DevBranch, Drupal Ukraine Community, Dropsolid
    - HitchShock
  - Digital Polygon
    - sarahjean
  - Drupal Association
    - fjgarlin
  - Fabb
    - AndyF
  - Factorial GmbH
    - D34dMan
    - joachim, also volunteer
    - kristiaanvandeneynde
  - Finalist
    - Arantxio
    - daffie
  - Flat 9
    - mrweiner
  - Fruition
    - benjifisher
  - Full Fat Things
    - longwave
  - ImageX
    - carolpettirossi
  - Lullabot
    - ckrina
    - deviantintegral
    - e0ipso
  - McGill University
    - fengtan
  - MD Systems GmbH
    - Berdir
  - Mediacurrent
    - DamienMcKenna
  - Minsky
    - DieterHolvoet
  - Mobomo
    - smustgrave
  - MTech, LLC
    - edysmp
    - heddn
  - Northern Commerce
    - bbombachini
  - OpenSense Labs
    - Nitin shrivastava
  - Open Social
    - Kingdutch
  - Opin
    - yogeshmpawar
  - Perficient
    - ctrlADel
  - PreviousNext
    - acbramley
    - kim.pepper
    - larowlan
    - mstrelan
    - quietone
    - Sam152
  - Salsa Digital
    - ameymudras
  - Savas Labs
    - dmurphy1
  - Skilld
    - andypost
  - Srijan
    - Akram Khan
    - kkalashnikov
    - Prem Suthar
    - Ranjit1032002
    - Rassoni
    - varun verma
  - Third and Grove, Tag1 Consulting
    - catch
  - Tobania
    - BramDriesen
  - Valuebound
    - sourabhjain
  - Volunteers
    - Alan D.
    - Albert Volkman
    - Chi
    - cilefen
    - donquixote
    - duadua
    - dww
    - Gábor Hojtsy
    - hchonov
    - herved
    - Mingsong
    - mondrake
    - mpellegrin
    - mr.baileys
    - jshimota01
    - Pancho
    - P44T
    - rclemings
    - royalpinto007
    - Sander Edwards van Muijen
    - seanB
    - sharkbaitdc
    - Spokje
    - S3b0uN3t
  - Webikon
    - claudiu.cristea
  - Zodiac Media
    - codebymikey
  - Zyxware Technologies
    - ranjith_kumar_k_u

### The set

    $ git log --oneline --reverse 23fc2b6c35..570710ad7b
    553666589a Issue #3221128 by longwave, duadua, bradjones1, andypost, smustgrave: Enable service autoconfiguration for core event subscribers
    80174a6c80 Issue #3365480 by ctrlADel, e0ipso, sharkbaitdc, smustgrave, sarahjean, carolpettirossi: [SDC] Improve error handling during prop validation errors
    3b6179f198 Issue #3332456 by Spokje, jastraat: [random test failure] MediaTest::testLinkManualDecorator()
    d5740e4ba1 Issue #3366287 by lauriii, catch, dmurphy1: [regression] Inserting media via the media library modal when paged redirects to the wrong destination
    26e376b1df Issue #3348093 by andypost, murilohp, elber, Ranjit1032002, ameymudras, smustgrave, alexpott, catch: Deprecate user_roles() and user_role_names()
    16d3fc11e0 Issue #3366314 by Prem Suthar, larowlan: Remove composer optimisations for laminas/laminas-zendframework-bridge
    d838d8c084 Issue #3366633 by Spokje: Uncomment assertions in StyleTest related to https://github.com/ckeditor/ckeditor5/issues/11709
    915fe89875 Issue #3270647 by P44T, AndyF, Sander Edwards van Muijen, Nitin shrivastava, Albert Volkman, mr.baileys, jshimota01, catch, mpellegrin, poker10, alexpott: PhpMail : broken mail headers in PHP 8.0+ because of LF characters
    f0a4f3f159 Issue #3319184 by royalpinto007, bbombachini, sourabhjain, Rassoni, smustgrave, Chi, catch, mstrelan: hook_condition_info_alter is not documented
    471c0afd6e Issue #3027705 by claudiu.cristea, Pancho, smustgrave: Allow ?edit[field_xyz] as query parameter in contextual filter
    085ae2c2fb Issue #3347873 by kristiaanvandeneynde, BramDriesen, smustgrave, Kingdutch, quietone, borisson_: Centralize permission checks in a service
    fdf05ab0de Issue #3346560 by DieterHolvoet, smustgrave: Allow extending StatusMessages class
    99c902a24f Issue #3366288 by andypost, longwave: Add [#\ReturnTypeWillChange] attribute to TemporaryArrayObjectThrowingExceptions for PHP 8.3 compatibility
    b83467eabc Issue #3325557 by longwave, mondrake, kim.pepper: Enable more service autowiring by adding interface aliases to core modules
    cd8430a3a6 Issue #3366081 by Spokje: [random test failure] MediaTest:: testEditableCaption()
    b1dd9f7e2f Issue #3355122 by joachim, dww, D34dMan: ContentTranslationContextualLinksTest should use API calls to set up translation
    ca17d0acb3 Issue #3366481 by cilefen, acbramley, phenaproxima, seanB: OEmbedIframeController returns an HTTP response code that can be cached by forward proxies when it is given illegal parameters
    2fc7e23e4a Issue #3346765 by catch, heddn, Akram Khan, Wim Leers, mondrake: Add PerformanceTestBase for allowing browser performance assertions within FunctionalJavaScriptTests
    fc024f3520 Issue #3309585 by Mingsong, Wim Leers, Chi: CKEditor 5 Style plugin configuration tab does not appear
    0448b4bcc7 Issue #3334489 by catch, Berdir, longwave, andypost: ChainedFastBackend invalidates all items when cache tags are invalidated
    68c1000f77 Issue #3052115 by huzooka, HitchShock, floydm, ranjith_kumar_k_u, Sam152, edysmp, codebymikey, herved, Wim Leers, quietone, hchonov, Berdir, DamienMcKenna, rclemings, donquixote, benjifisher, fengtan, jwilson3: Mark an entity as 'syncing' during a migration update
    a87a72fa06 Issue #3275557 by andypost, catch, heddn: Add webp image conversion to core's install profile's image style
    412f414dc7 Revert "Issue #3052115 by huzooka, HitchShock, floydm, ranjith_kumar_k_u, Sam152, edysmp, codebymikey, herved, Wim Leers, quietone, hchonov, Berdir, DamienMcKenna, rclemings, donquixote, benjifisher, fengtan, jwilson3: Mark an entity as 'syncing' during a migration update"
    d785011603 Issue #3366722 by Gábor Hojtsy, lauriii: Add Lauri Eskola to Drupal core product managers
    6c1e7b0774 Issue #3256642 by mondrake, daffie, yogeshmpawar, alexpott, tmaiochi, quietone, larowlan, catch: Introduce database driver extensions and autoload database drivers' dependencies
    80aa96f173 Issue #3355403 by mglaman: Add "Edit permisisons" as local task on role edit form
    f87821dd0a Issue #3129669 by smustgrave, larowlan, S3b0uN3t: Block content set contextual edit links to edit_form route instead of canonical
    e0cbbca62e Issue #3052115 by huzooka, HitchShock, floydm, ranjith_kumar_k_u, Sam152, edysmp, codebymikey, herved, Wim Leers, quietone, hchonov, Berdir, DamienMcKenna, rclemings, donquixote, benjifisher, fengtan, jwilson3: Mark an entity as 'syncing' during a migration update
    381c38a927 Issue #3359240 by Utkarsh_33, narendraR, deviantintegral, lauriii, tim.plunkett, smustgrave, joachim, ckrina, hooroomoo: Enable bundle selection when a new view mode is created
    8b92bea8c7 Issue #3356515 by fjgarlin, varun verma, smustgrave: Give users a way to access announcements if toolbar module is disabled
    224543f183 Issue #3351886 by Arantxio, daffie, mondrake, longwave: Change SchemaUniquePrefixedKeysIndexTest to extend DriverSpecificDatabaseTestBase
    0a6ff2635d Issue #3261574 by quietone, kkalashnikov, alexpott, smustgrave: Add .engine to files scanned by PHPCS
    8b85a04407 Issue #3360139 by bradjones1: Phpdoc for ResourceTypeRepositoryInterface::get return value is incomplete
    b7c4aaf2e0 Issue #3361465 by Spokje: Remove outdated @todo's pointing to #3135457
    570710ad7b Issue #2662330 by Utkarsh_33, lauriii, hooroomoo, bnjmnm, tim.plunkett, Alan D., smustgrave, catch, mrweiner: Machine name generation is way too slow
    
    $ git diff --shortstat 23fc2b6c35..570710ad7b
     206 files changed, 3087 insertions(+), 591 deletions(-)
