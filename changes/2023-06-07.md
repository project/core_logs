---
date: "2023-06-07"
title: "2023-06-07: Two DrupalCon days"
---

This seems to be becoming recurrent, hopefully I can turn this into a
sustainable effort at some point.

## Changes

It covers commits added over the `7598b15a28..c8806a0299` interval on `11.x` branch.

### #3354382: [PHPUnit 10] Provide a static viable alternative to $this->prophesize() in data providers

[#3354382][3354382]

This happens in the context of [PHPUnit 10 support meta][3217904], to be
compatible with that new version, where `@dataProvider` is only allowed to use
static methods.

[3354382]: https://www.drupal.org/project/drupal/issues/3354382
[3217904]: https://www.drupal.org/project/drupal/issues/3217904

### #3346748: Entering a non-numeric value for a start row value in 'Multiple field settings' for a views field leads to a fatal error

[#3346748][3346748]

Bug fix for views field multiple values setting :+1:

[3346748]: https://www.drupal.org/project/drupal/issues/3346748

### #3270148: Provide a mechanism for deprecation of variables used in twig templates

[#3270148][3270148]

Deprecations are useful to signal clearly what is going to change.
On general PHP code, that is usually methods, function calls, or even other
things like classes, but there are many specific concepts in Drupal that are
less trivial.

Here, a change to allow declaring a given twig template variable as deprecated
has been added.
More information at the [Twig variables can be deprecated][3334622] change
record.

[3270148]: https://www.drupal.org/project/drupal/issues/3270148
[3334622]: https://www.drupal.org/node/3334622

### #2551419: Abstract RenderCache into a separate service that is capable of cache redirects in a non-render array-specific way

[#2551419][2551419]

Drupal cache implementation has been rewritten around `8.x`, and it has been
iteratively improved.

This change tries to generalize part of the implementation to be more reusable.
At the same time, per notes on the issue this has improved rendering performance
too.

Almost eight years in the works, it is now in!
Some extra information on the related [EntityCacheTagsTestBase::createCacheId
has been removed][3354596] change record.

[2551419]: https://www.drupal.org/project/drupal/issues/2551419
[3354596]: https://www.drupal.org/node/3354596

### #3364713: Claro: Messages can be malformed when JS creates messages and PHP messages already exist

[#3364713][3364713]

Bug fix for drupal messages on the Claro theme, following Olivero fix at [#3223264][3223264].

[3223264]: https://www.drupal.org/project/drupal/issues/3223264
[3364713]: https://www.drupal.org/project/drupal/issues/3364713

## Closing

This time, I found a non-trivial cache subsystem improvement, one extra prepare
change for PHPUnit 10, an extra way to declare a deprecation, and a couple of
bug fixes.

It is nice to see this activity, and it is important to highlight that many
people are involved on each of the issues, that's a vibrant community in action.

It is also worth to mention that an effort along almost 8 years has been closed
on the cache issue, that may not be ideal, but it is just great to see the fruit
of that collaboration arriving into the main codebase.

## Annex

### Meta

- Commits: 5
- Approach:
  - Direct commit: 0
  - Merge request + Patch: 1
  - Merge request: 2
  - Patch: 2
- Days: 2, DrupalCon happening
- Shortest running issue: ~3 days
- Longest running issue: ~8 years
- People: 22
- Volunteers: 4
- Organizations: 16
  - Acquia
    - bnjmnm
    - lauriii
    - Wim Leers
  - Calibrate
    - borisson_
  - Chapter Three
    - dawehner
  - CI&T
    - murilohp
  - Factorial
    - kristiaanvandeneynde
  - Full Fat Things
    - longwave
  - iO
    - Lendude
  - Lullabot
    - Dave Reid
  - Mobomo
    - smustgrave
  - Nerdery
    - cosmicdreams
  - PreviousNext
    - larowlan
    - quietone
  - Skilld
    - andypost
  - Third and Grove
    - catch
  - Thunder
    - alexpott
  - ems.education software, Not Vanilla
    - bradjones1
  - Volunteers
    - andy-blum
    - jonathanshaw
    - mondrake
    - tstoeckler

### The set

    $ git log --oneline --reverse 7598b15a28..c8806a0299
    6ef863a2a3 Issue #3354382 by mondrake: [PHPUnit 10] Provide a static viable alternative to $this->prophesize() in data providers
    d3d1d05dfa Issue #3346748 by Lendude, borisson_: Entering a non-numeric value for a start row value in 'Multiple field settings' for a views field leads to a fatal error
    3c8128a0ac Issue #3270148 by jonathanshaw, murilohp, catch, smustgrave, longwave, larowlan, andypost, alexpott, quietone: Provide a mechanism for deprecation of variables used in twig templates
    339643bebb Issue #2551419 by kristiaanvandeneynde, catch, dawehner, Wim Leers, borisson_, bradjones1, tstoeckler, andypost: Abstract RenderCache into a separate service that is capable of cache redirects in a non-render array-specific way
    c8806a0299 Issue #3364713 by lauriii, bnjmnm, andy-blum, Dave Reid, cosmicdreams: Claro: Messages can be malformed when JS creates messages and PHP messages already exist

    $ git diff --shortstat 7598b15a28..c8806a0299
     68 files changed, 1930 insertions(+), 901 deletions(-)
