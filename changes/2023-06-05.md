---
date: "2023-06-05"
title: "2023-06-05: Three days go by"
---

A third iteration, first multi-day.

## Changes

It covers commits added over the `b8e7644636..7598b15a28` interval on `11.x` branch.

### #3363222: Update to Symfony 6.3

[#3363222][3363222]

A clean vendor upgrade of Symfony components from 6.3-rc1 to 6.3, that even
include a few security updates.
It is nice to see one of the main Drupal dependencies is being kept up-to-date.

[3363222]: https://www.drupal.org/project/drupal/issues/3363222

### A couple of temporary work-around changes to avoid test to fail on postgres

It has been a while since I see a couple of commit messages without issue
references, but looking at the details, two commits, that actually add a code
link to the codebase, and skip postgres on testing were added.

This seems like a quick fix to avoid disrupting all other changes, and keep
mainline on green.

BTW, the related liked issue, at this point RTBC is [#3364621][3364621].

[3364621]: https://www.drupal.org/project/drupal/issues/3364621

### Redirects for changed block content related admin paths

Block content module admin request paths changed a bit recently.
This change takes care of the back-wards compatible layer, in this case
redirecting the old paths to the new ones.

This will certainly save the day for drupal admins with bookmarks or that rely
on browser history, :+1:

[#3351750][3351750]

[3351750]: https://www.drupal.org/project/drupal/issues/3351750

### webchick steps down

[#3363391][3363391]

:sad_panda:

Dear Angie, thanks so much for helping Drupal all these years!
You made amazing work, and above all you made a huge difference in the life of
so many of us with your involvement in this project.

This is an issue when you can see the human side of the project.

Drupal core, is a huge open source project, and even if the Drupal community has
an excellent track of highly involved people over long periods of time, it s
natural to see people moving to different projects.

Many of us will miss a lot webchick, but her ethos will definitely live on
through many of us!

[3363391]: https://www.drupal.org/project/drupal/issues/3363391

### #3364088: Ajax state leaking to Views destination paths

[#3364088][3364088]

Bug-fix for interaction between AJAX and views.
I'm so grateful glad these fixes are getting in without encountering them.
Drupal is doing lots of things, so these kind of

[3364088]: https://www.drupal.org/project/drupal/issues/3364088

### #3039185: Allow field blocks to display the configuration label when set in Layout Builder

[#3039185][3039185]

Bug-fix for layout builder to correctly display an overridden title.

[3039185]: https://www.drupal.org/project/drupal/issues/3039185

## Closing

A nice combination of keeping up-to-date, adding bug fixes, back-ward
compatibility additions, and mainly a vividly nostalgia of missing webchick
already.

## Annex

### Meta

- Commits: 7
- Approach:
  - Direct commit: 2
  - Merge request + Patch: 0
  - Merge request: 1
  - Patch: 4
- Days: 3
- Shortest running issue: < 4 days
- Longest running issue: > 4 years
- People: 18
- Volunteers: 5
- Organizations: 13
  - Acquia
    - lauriii
    - tim.plunkett
  - Agiledrop
    - nkoporec
  - eps & kaas
    - swentel
  - Faichi Solutions Pvt Ltd
    - anup.singh
  - Mobomo
    - smustgrave
  - MongoDB
    - webchick
  - PreviousNext
    - larowlan
  - Princeton University
    - bkosborne
  - QED42
    - yogeshmpawar
  - Skilld
    - andypost
  - Srijan
    - Rassoni
  - Tag1 Consulting
    - benjifisher
  - Third and Grove
    - catch
  - Volunteer
    - AaronMcHale
    - olli
    - nord102
    - manishsaharan

### The set

    $ git log --oneline --reverse b8e7644636..7598b15a28
    96115eaa81 Issue #3363222 by andypost: Update to Symfony 6.3
    f1c28793d2 Skip \Drupal\Tests\file\Kernel\Views\RelationshipNodeFileDataTest on PostgreSQL
    99b1114f57 Skip \Drupal\Tests\file\Kernel\Views\RelationshipUserFileDataTest on PostgreSQL
    970abeb480 Issue #3351750 by benjifisher, Rassoni, smustgrave, larowlan, AaronMcHale: Create BC redirects for children of changed paths
    b0df8f6de3 Issue #3363391 by webchick: Remove webchick from MAINTAINERS.txt
    710278ccea Issue #3364088 by lauriii, tim.plunkett, anup.singh, olli: Ajax state leaking to Views destination paths
    7598b15a28 (HEAD -> reading, origin/HEAD, origin/11.x) Issue #3039185 by nord102, yogeshmpawar, swentel, tim.plunkett, manishsaharan, nkoporec, catch, bkosborne: Allow field blocks to display the configuration label when set in Layout Builder
