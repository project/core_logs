# Schedule

The target is to have around three change reports per minor release, and one
development statistics report per minor release.

That follows [core release cycle][core-release-cycle], making change reports
happen every two months, and the development statistics report once every six
months, after the minor release.

## Next

Dates here are mainly hints for action, and not exact expected dates.

- 2023-06-05: First ever change report, likely based on `10.1.0-rc1`, `10.1.x`
part 1.
- 2023-06-26: Change report based on `10.1.0`, `10.1.x` part 2.
- 2023-06-26: First ever development statistics report, around `10.1.0`.


[core-release-cycle]: https://www.drupal.org/about/core/policies/core-release-cycles/schedule
